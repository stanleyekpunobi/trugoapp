﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using TrugoAdmin.Models.Classes;
using TruGoClassLibrary.RequestModels;
using TruGoClassLibrary.ResponseClass;

namespace TrugoAdmin.Models.DalClasses
{
    public class InvoiceRequestClass
    {
        public List<InvoiceResponseModel> GetAllRequest()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetAllInvoices", Method.GET);

            //request.AddHeader("bearer +", "");

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<InvoiceResponseModel>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }


        public bool ApproveRequest(InvoiceRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/ApproveInvoice", Method.POST);

            var parameters = new Dictionary<string, string>();

            parameters.Add("Invoiceid", Convert.ToString(model.Invoiceid));

            parameters.Add("Code", model.Code.ToString());

            parameters.Add("UserId", model.UserId);

            request.AddHeader("content-type", "application/json");

            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool VerifyRequest(InvoiceRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/VerifyInvoice", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("Code", model.Code.ToString());
            parameters.Add("Invoiceid", Convert.ToString(model.Invoiceid));
            parameters.Add("UserId", Convert.ToString(model.UserId));
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool RevokeVerifiedRequest(InvoiceRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/RevokeInvoiceVerification", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.UserId);
            parameters.Add("Invoiceid", Convert.ToString(model.Invoiceid));
            parameters.Add("Code", Convert.ToString(model.Code));
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeclineRequest(InvoiceRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/DeclineInvoice", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.UserId);
            parameters.Add("Invoiceid", Convert.ToString(model.Invoiceid));
            parameters.Add("Code", Convert.ToString(model.Code));
            parameters.Add("Message", Convert.ToString(model.Message));
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}