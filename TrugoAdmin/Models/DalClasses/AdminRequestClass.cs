﻿using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Net;
using TrugoAdmin.Models.Classes;
using TruGoClassLibrary.RequestModels;
using TruGoClassLibrary.ResponseClass;

namespace TrugoAdmin.Models.DalClasses
{
    public class AdminRequestClass
    {
        public UsersResponseModel GetAllUsers()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetAllUsers", Method.GET);
            
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<UsersResponseModel>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public UsersResponseModel GetPendingUsers()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetPendingUsers", Method.GET);

            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<UsersResponseModel>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        
        public UsersResponseModel GetVerifiedUsers()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetVerifiedUsers", Method.GET);

            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<UsersResponseModel>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public UsersResponseModel GetApprovedUsers()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetApprovedUsers", Method.GET);

            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<UsersResponseModel>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public bool ApproveUser(string userid)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/ApproveUserProfile", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", userid);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool VerifyUser(string userid)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/VerifyUserProfile", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", userid);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddAdmin(AddAdminRequestLibraryModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/AddAdmin", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.UserId);
            parameters.Add("Role", model.Role);

            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeclineUser(string userid, string message)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/DeclineUserProfile", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", userid);
            parameters.Add("Message", message);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool RevokeUserVerification(string userid)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/RevokeUserProfile", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", userid);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
    
}
         


    
