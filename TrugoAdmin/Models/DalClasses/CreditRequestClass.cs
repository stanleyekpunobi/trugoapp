﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using TrugoAdmin.Models.Classes;
using TruGoClassLibrary.RequestModels;
using TruGoClassLibrary.ResponseClass;

namespace TrugoAdmin.Models.DalClasses
{
    public class CreditRequestClass
    {
        public List<CreditResponseModel> GetAllRequest()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetCreditRequests", Method.GET);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<CreditResponseModel>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public List<CreditResponseModel> GetVerifiedRequests()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetVerifiedCreditRequest", Method.GET);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<CreditResponseModel>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public List<CreditResponseModel> GetApprovedRequests()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetApprovedCreditRequests", Method.GET);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<CreditResponseModel>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public List<CreditResponseModel> GetPendingRequests()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetPendingCreditRequests", Method.GET);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<CreditResponseModel>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public bool ApproveRequest(CreditLibraryRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/ApproveCreditRequest", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.Userid);
            parameters.Add("Id", Convert.ToString(model.Id));
            parameters.Add("CreditAmount", Convert.ToString(model.CreditAmount));

            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool VerifyRequest(CreditLibraryRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/VerifyCreditRequest", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.Userid);
            parameters.Add("Id", Convert.ToString(model.Id));
            parameters.Add("CreditAmount", Convert.ToString(model.CreditAmount));
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeclineRequest(CreditLibraryRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/DeclineCreditRequest", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.Userid);
            parameters.Add("Id", Convert.ToString(model.Id));
            parameters.Add("CreditAmount", Convert.ToString(model.CreditAmount));
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool RevokeVerifiedRequest(CreditLibraryRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/RevokeVerifiedCreditRequest", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.Userid);
            parameters.Add("Id", Convert.ToString(model.Id));
            parameters.Add("CreditAmount", Convert.ToString(model.CreditAmount));
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}