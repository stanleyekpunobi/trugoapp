﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using TrugoAdmin.Models.Classes;
using TruGoClassLibrary.RequestModels;
using TruGoClassLibrary.ResponseClass;

namespace TrugoAdmin.Models.DalClasses
{
    public class WithdrawalRequestClass
    {
        public List<WithdrawCreditResponse> GetAllRequest()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetWithdrawalRequests", Method.GET);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<WithdrawCreditResponse>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public List<WithdrawCreditResponse> GetVerifiedRequests()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetVerifiedWithdrawalRequests", Method.GET);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<WithdrawCreditResponse>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public List<WithdrawCreditResponse> GetApprovedRequests()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetApprovedWithdrawalRequests", Method.GET);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<WithdrawCreditResponse>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public List<WithdrawCreditResponse> GetPendingRequests()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Admin/GetPendingWithdrawalRequests", Method.GET);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<WithdrawCreditResponse>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public bool ApproveRequest(CreditLibraryRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/ApproveWithdrawalRequest", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.Userid);
            parameters.Add("Id", Convert.ToString(model.Id));
            parameters.Add("CreditAmount", Convert.ToString(model.CreditAmount));

            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool VerifyRequest(CreditLibraryRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/VerifyWithdrawalRequest", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.Userid);
            parameters.Add("Id", Convert.ToString(model.Id));
            parameters.Add("CreditAmount", Convert.ToString(model.CreditAmount));
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeclineRequest(CreditLibraryRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/DeclineWithdrawalRequest", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.Userid);
            parameters.Add("Id", Convert.ToString(model.Id));
            parameters.Add("CreditAmount", Convert.ToString(model.CreditAmount));
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool RevokeVerifiedRequest(CreditLibraryRequestModel model)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);
            var request = new RestRequest("api/Admin/RevokeVerifiedRequest", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", model.Userid);
            parameters.Add("Id", Convert.ToString(model.Id));
            parameters.Add("CreditAmount", Convert.ToString(model.CreditAmount));
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}