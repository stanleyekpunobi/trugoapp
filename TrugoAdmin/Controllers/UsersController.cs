﻿using System.Web.Mvc;
using TrugoAdmin.Models.DalClasses;
using TruGoClassLibrary.RequestModels;

namespace TrugoAdmin.Controllers
{
    public class UsersController : Controller
    {
        private AdminRequestClass _adminRequestClass;

        public UsersController()
        {

        }

        public UsersController(AdminRequestClass adminRequestClass)
        {
            AdminRequestClass = adminRequestClass;
        }

        public AdminRequestClass AdminRequestClass
        {
            get
            {
                return new AdminRequestClass();
            }

            set
            {
                _adminRequestClass = value;
            }
        }

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Viewusers()
        {

            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var users = AdminRequestClass.GetAllUsers();

            if (users != null)
            {
                return View(users);

            }
            else
            {
                return View();
            }
        }

        public ActionResult Pendingusers()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var users = AdminRequestClass.GetPendingUsers();

            if (users != null)
            {
                return View(users);

            }
            else
            {
                return View();
            }
        }

        public ActionResult Approvedusers()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var users = AdminRequestClass.GetApprovedUsers();

            if (users != null)
            {
                return View(users);

            }
            else
            {
                return View();
            }
        }

        public ActionResult Verifiedusers()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var users = AdminRequestClass.GetVerifiedUsers();

            if (users != null)
            {
                return View(users);

            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult VerifyUser(string userid)
        {
            var status = AdminRequestClass.VerifyUser(userid);

            if (status)
            {
                Session["msg"] = "User profile verification approved";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("Verifiedusers");
        }

        [HttpPost]
        public ActionResult RevokeVerification(string userid)
        {
            var status = AdminRequestClass.RevokeUserVerification(userid);

            if (status)
            {
                Session["msg"] = "User profile verification revoked";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("Verifiedusers");
        }

        public ActionResult Addadmin()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            return View();
        }

        [HttpPost]
        public ActionResult ApproveUser(string userid)
        {
            var approved = AdminRequestClass.ApproveUser(userid);

            if (approved == true)
            {
                Session["msg"] = "User profile approved";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("Addadmin");
        }

        

        [HttpPost]
        public ActionResult AddAdminUser(AddAdminRequestLibraryModel model)
        {
            var approved = AdminRequestClass.AddAdmin(model);

            if (approved == true)
            {
                Session["msg"] = "User added as"+ model.Role;
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("Viewusers");
        }

        [HttpPost]
        public ActionResult DeclineRequest(string userid, string additionalmessage)
        {
            var isdeclined = AdminRequestClass.DeclineUser(userid, additionalmessage);
            if (isdeclined == true)
            {
                Session["msg"] = "User profile declined";
            }
            else
            {
                Session["msg"] = "An error occured";

            }

            return RedirectToAction("Viewusers");
        }
    }
}