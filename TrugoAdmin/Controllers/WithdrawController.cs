﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrugoAdmin.Models.DalClasses;
using TruGoClassLibrary.RequestModels;

namespace TrugoAdmin.Controllers
{
    public class WithdrawController : Controller
    {
        private WithdrawalRequestClass _withdrawalRequestClass;

        public WithdrawalRequestClass WithdrawalRequestClass
        {
            get
            {
                return new WithdrawalRequestClass();
            }

            set
            {
                _withdrawalRequestClass = value;
            }
        }

        public WithdrawController()
        {

        }
        public WithdrawController(WithdrawalRequestClass withdrawalRequestClass)
        {
            WithdrawalRequestClass = withdrawalRequestClass;
        }



        // GET: Withdraw
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetallRequests()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = WithdrawalRequestClass.GetAllRequest();

            return View(requests);
        }

        public ActionResult GetVerifiedRequests()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = WithdrawalRequestClass.GetVerifiedRequests();

            return View(requests);
        }

        public ActionResult GetApprovedRequests()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = WithdrawalRequestClass.GetApprovedRequests();

            return View(requests);
        }

        public ActionResult GetPendingRequests()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = WithdrawalRequestClass.GetPendingRequests();

            return View(requests);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveRequest(CreditLibraryRequestModel model)
        {
            var status = WithdrawalRequestClass.ApproveRequest(model);

            if (status)
            {
                Session["msg"] = "Withdrawal request approved";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("GetallRequests");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VerifyRequest(CreditLibraryRequestModel model)
        {
            var status = WithdrawalRequestClass.VerifyRequest(model);

            if (status)
            {
                Session["msg"] = "Withdrawal request verified";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("GetVerifiedRequests");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeclineRequest(CreditLibraryRequestModel model)
        {
            var status = WithdrawalRequestClass.DeclineRequest(model);

            if (status)
            {
                Session["msg"] = "Withdrawal request declined";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("GetallRequests");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RevokeVerifiedRequest(CreditLibraryRequestModel model)
        {
            var status = WithdrawalRequestClass.RevokeVerifiedRequest(model);

            if (status)
            {
                Session["msg"] = "Withdrawal request revoked";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("GetallRequests");
        }
    }
}