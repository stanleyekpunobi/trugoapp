﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrugoAdmin.Models.DalClasses;
using TruGoClassLibrary.RequestModels;

namespace TrugoAdmin.Controllers
{
    public class CreditController : Controller
    {
        private CreditRequestClass _creditRequestClass;

        public CreditRequestClass CreditRequestClass
        {
            get
            {
                return new CreditRequestClass();
            }

            set
            {
                _creditRequestClass = value;
            }
        }

        public CreditController(CreditRequestClass creditRequestClass)
        {
            CreditRequestClass = creditRequestClass;
        }

        public CreditController()
        {

        }

        public ActionResult GetallRequests()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = CreditRequestClass.GetAllRequest();

            return View(requests);
        }

        public ActionResult GetVerifiedRequests()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = CreditRequestClass.GetVerifiedRequests();

            return View(requests);
        }

        public ActionResult GetApprovedRequests()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = CreditRequestClass.GetApprovedRequests();

            return View(requests);
        }

        public ActionResult GetPendingRequests()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = CreditRequestClass.GetPendingRequests();

            return View(requests);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveRequest(CreditLibraryRequestModel model)
        {
            var status = CreditRequestClass.ApproveRequest(model);

            if (status)
            {
                Session["msg"] = "Withdrawal request approved";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("GetallRequests");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VerifyRequest(CreditLibraryRequestModel model)
        {
            var status = CreditRequestClass.VerifyRequest(model);

            if (status)
            {
                Session["msg"] = "Withdrawal request verified";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("GetVerifiedRequests");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeclineRequest(CreditLibraryRequestModel model)
        {
            var status = CreditRequestClass.DeclineRequest(model);

            if (status)
            {
                Session["msg"] = "Withdrawal request declined";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("GetallRequests");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RevokeVerifiedRequest(CreditLibraryRequestModel model)
        {
            var status = CreditRequestClass.RevokeVerifiedRequest(model);

            if (status)
            {
                Session["msg"] = "Withdrawal request revoked";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("GetallRequests");
        }
    }
}