﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrugoAdmin.Models.DalClasses;
using TruGoClassLibrary.RequestModels;

namespace TrugoAdmin.Controllers
{
    public class InvoicesController : Controller
    {
        private InvoiceRequestClass _invoiceRequestClass;

        public InvoiceRequestClass InvoiceRequestClass
        {
            get
            {
                return new InvoiceRequestClass();
            }

            set
            {
                _invoiceRequestClass = value;
            }
        }

        public InvoicesController(InvoiceRequestClass invoiceRequestClass)
        {
            InvoiceRequestClass = invoiceRequestClass;
        }

        public InvoicesController()
        {

        }
        // GET: Invoices
        public ActionResult GetAllInvoices()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = InvoiceRequestClass.GetAllRequest();

            return View(requests);
        }

        public ActionResult PendingInvoices()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = InvoiceRequestClass.GetAllRequest().Where(m => m.Verified == false || m.AdminStatus == "not approved");
            
            return View(requests);
        }

        public ActionResult VerifiedInvoices()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = InvoiceRequestClass.GetAllRequest().Where(m => m.Verified == true);

            return View(requests);
        }

        public ActionResult ApprovedInvoices()
        {
            if (Session["msg"] != null)
            {
                ViewBag.msg = Session["msg"].ToString();
                Session.Remove("msg");
            }
            else { }

            var requests = InvoiceRequestClass.GetAllRequest().Where(m => m.Verified == true && m.AdminStatus == "approved");

            return View(requests);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveRequest(InvoiceRequestModel model)
        {
            var status = InvoiceRequestClass.ApproveRequest(model);

            if (status)
            {
                Session["msg"] = "Invoice request approved";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("ApprovedInvoices");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VerifyRequest(InvoiceRequestModel model)
        {
            var status = InvoiceRequestClass.VerifyRequest(model);

            if (status)
            {
                Session["msg"] = "Invoice request verified";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("VerifiedInvoices");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeclineRequest(InvoiceRequestModel model)
        {
            var status = InvoiceRequestClass.DeclineRequest(model);

            if (status)
            {
                Session["msg"] = "Invoice request declined";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("PendingInvoices");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RevokeVerifiedRequest(InvoiceRequestModel model)
        {
            var status = InvoiceRequestClass.RevokeVerifiedRequest(model);

            if (status)
            {
                Session["msg"] = "Invoice request revoked";
            }
            else
            {
                Session["msg"] = "An error occured";
            }

            return RedirectToAction("GetAllInvoices");
        }

    }
}