var FormInputMask = function () {
    
    var handleInputMasks = function () {

        $("#mask_phone").inputmask("mask", {
            "mask": "(999) 999-9999-999",
            "initialCountry":"Nigeria"
        }); //specifying fn & options

    };

 
    return {
        //main function to initiate the module
        init: function () {
            handleInputMasks();
        }
    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        FormInputMask.init(); // init metronic core componets
    });
}