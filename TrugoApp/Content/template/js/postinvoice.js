﻿function CalculateInvoiceDiscount(invoicetotal, discount) {

    if (invoicetotal === "") {
        invoicetotal = 0;
    }
    if (discount === "") {
        discount = 0;
    }

    var percentagediscount = discount / 100;

    var discountprice = invoicetotal * percentagediscount;

    var totalprice = invoicetotal - discountprice;

    return totalprice;
}

$("#txtinvoiceamount").bind('keyup mouseup', function () {

    var invoicediscount = $("#txtinvoicedisc").val();

    var invoiceamount = $("#txtinvoiceamount").val();

    var price = CalculateInvoiceDiscount(invoiceamount, invoicediscount)


    $("#txtinvoicesaleprice").val(price);

});
$("#txtinvoicedisc").bind('keyup mouseup', function () {

    var invoicediscount = $("#txtinvoicedisc").val();

    var invoiceamount = $("#txtinvoiceamount").val();

    var price = CalculateInvoiceDiscount(invoiceamount, invoicediscount)


    $("#txtinvoicesaleprice").val(price);

});

$("#btnsubmit").click(function () {
    toastr.success('Your invoice has been created and under moderation by TruGo suppoort team', 'Invoice successfuly created', { timeOut: 4000 });

    setTimeout(function () {
        window.location.replace("http://localhost:64178/Seller/FeatureInvoice");
    }, 5000)
});
