﻿function CalculateInvoicePurchasePercentage(balance, amount) {

    if (balance === "") {
        balance = 0;
    }
    if (amount === "") {
        amount = 0;
    }

    var percentageprofit = amount / balance * 100;
     

    return percentageprofit;
}

function CalculateInvoicePurchaseAmount(balance, percentage) {

    if (balance === "") {
        balance = 0;
    }
    if (percentage === "") {
        percentage = 0;
    }

    var purchaseamount = (percentage / 100) * balance;


    return purchaseamount;
}

function CalculateProfit(saleprice, totalprice, purchasepercentage) {

    if (saleprice === "") {
        saleprice = 0;
    }

    if (totalprice === "") {
        totalprice = 0;
    }

    if (purchasepercentage === "") {
        purchasepercentage = 0;
    }

    var saledifference = totalprice - saleprice;

    var getprofit = purchasepercentage / 100 * saledifference;

    return getprofit;

}

$("#txtinvoicepurchaseamt").bind('keyup mouseup', function () {

    var balance = $("#txtinvoicebalance").val();

    var amount = $("#txtinvoicepurchaseamt").val();

    var totalprice = $("#txtinvoiceamount").val();

    var price = CalculateInvoicePurchasePercentage(balance, amount);

    var profit = CalculateProfit(balance, totalprice, price);

    $("#txtinvoicepurchasedisc").val(price);

    $("#txtpurchaseprofit").val(profit);

});
$("#txtinvoicepurchasedisc").bind('keyup mouseup', function () {

    var invoicediscount = $("#txtinvoicepurchasedisc").val();

    var invoicebalance = $("#txtinvoicebalance").val();

    var totalprice = $("#txtinvoiceamount").val();
    
    var amount = CalculateInvoicePurchaseAmount(invoicebalance, invoicediscount);

    var profit = CalculateProfit(invoicebalance, totalprice, invoicediscount);


    $("#txtinvoicepurchaseamt").val(amount);

    $("#txtpurchaseprofit").val(profit);

});