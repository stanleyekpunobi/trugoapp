﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace TrugoApp.Models.Classes
{
    public class EnvironmentClass
    {
        //set environment to dev or live
        private static bool IsAppTest { get; set; } = false;

        //api base url to use
        public static string ApiBaseUrl
        {
            get
            {
                return ConfigurationManager.AppSettings.Get(GetApiBaseUrl(IsAppTest));
            }
        }


        //return api url based on environment
        private static string GetApiBaseUrl(bool isAppTest)
        {
            if (isAppTest == true)
            {
                return "DevUrl";
            }
            else
            {
                return "LiveUrl";
            }
        }
    }
}