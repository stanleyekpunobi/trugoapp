﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using TrugoApp.Models.Classes;
using TruGoClassLibrary.ResponseClass;

namespace TrugoApp.Models.DalClasses
{
    public class InvestorDal
    {
        public InvestorDashboardResponseModel GetInvestorDashboardData(string userid)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Investor/GetDashboardData", Method.GET);

            request.AddQueryParameter("userid", userid);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<InvestorDashboardResponseModel>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }
    }
}