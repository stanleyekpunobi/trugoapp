﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using TrugoApp.Models.Classes;
using TruGoClassLibrary.RequestModels;
using TruGoClassLibrary.ResponseClass;

namespace TrugoApp.Models.DalClasses
{
    public class MarketPlaceDal
    {
        public InvoiceDetailResponseModel GetInvoiceDetail(int? invoiceid, string code)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Invoice/GetInvoiceDetail", Method.GET);

            request.AddQueryParameter("invoiceid", Convert.ToString(invoiceid));

            request.AddQueryParameter("code", code);
            
            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<InvoiceDetailResponseModel>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public List<InvoiceResponseModel> LoadInvoices()
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Invoice/LoadInvoices", Method.GET);

            var response = client.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<List<InvoiceResponseModel>>(response.Content);

            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
            {
                return responseContent;
            }
            else
            {
                return null;
            }
        }

        public List<InvoiceResponseModel> FilterInvoices(List<InvoiceResponseModel> invoices, InvoiceSearchRequestModel model)
        {
            if (!string.IsNullOrEmpty(model.SearchTerm))
            {
                invoices = invoices.Where(m => m.InvoiceTitle.Contains(model.SearchTerm)).ToList();
            }
            if (!string.IsNullOrEmpty(model.Date))
            {
                var invoiceDate = Convert.ToDateTime(model.Date);

                invoices = invoices.Where(m => m.Date_Created >= invoiceDate && m.Date_Created <= invoiceDate).ToList();
            }
            if (model.Invoicemax != 0)
            {
                
                invoices = invoices.Where(m => m.Amount >= model.Invoicemin && m.Amount <= model.Invoicemax).ToList();
            }
            if (model.Discount != 0)
            {

                invoices = invoices.Where(m => m.DiscountRate == model.Discount).ToList();
            }

            return invoices;
        }


    }
}