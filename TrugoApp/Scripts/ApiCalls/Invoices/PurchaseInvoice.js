﻿var apiUrl = 'http://apitest.trugo.co';
$("#btnpurchaseinvoice").click(function () {
    $("#loader").addClass('is-active');
    //$("#loader").attr('data-text', 'Setting up your TruGo profile');
    //$("#loader").attr('data-blink');
    var getUserDetails = localStorage.getItem('userdata');
    if (getUserDetails === null || getUserDetails === undefined) {
        swal("TruGo", "Unauthorized user access", "error").then(() => {
            window.location.replace("http://apptest.trugo.co/Account/Login");
        });
    }
    else {
        var userData = JSON.parse(getUserDetails);
        var invoiceid = $("#invoiceid").val();
        var code = $("#invoicecode").val();
        var amount = $("#txtinvoicepurchaseamt").val();
        var invoicediscount = $("#txtinvoicepurchasedisc").val();

        var requestObject = {
            ProfileId: userData.userId, InvoiceId: invoiceid, InvoicePercentage: invoicediscount, Amount: amount, Code: code
        };
        
        $.ajax({
            type: 'POST',
            url: apiUrl + '/api/Investor/PurchaseInvoice',
            data: JSON.stringify(requestObject),
            contentType: 'application/json',
            success: function (data) {
                console.log(data);
                $("#loader").removeClass('is-active');
                if (data !== undefined || data !== null) {
                    
                    if (data.status === true) {
                        swal("TruGo", "Invoice successfuly purchased").then(() => {
                            window.location.replace("http://apptest.trugo.co/Investor/Dashboard?userid" + getUserDetails.userId);
                        });
                    }
                    else {
                        swal("TruGo", "An error occured", "error");
                    }
                }
            },
            error: function (code, exception) {
                $("#loader").removeClass('is-active');
                if (code.status === 0) {
                    swal("TruGo", "You are not connected to the internet", "error");
                } else if (code.status === 404) {
                    swal("TruGo", "an error has occured", "error");
                } else if (code.status === 500) {
                    swal("TruGo", "an error has occured", "error");
                } else if (exception === 'parsererror') {
                    swal("TruGo", "an error has occured", "error");
                } else if (exception === 'timeout') {
                    swal("TruGo", "Request has timed out", "error");
                } else if (exception === 'abort') {
                    swal("TruGo", "an error has occured", "error");
                } else {
                    swal("TruGo", "an error has occured", "error");
                }

            }
        });

    }
});

function CalculateInvoicePurchasePercentage(balance, amount) {

    if (balance === "") {
        balance = 0;
    }
    if (amount === "") {
        amount = 0;
    }

    var percentageprofit = amount / balance * 100;


    return percentageprofit;
}

function CalculateInvoicePurchaseAmount(balance, percentage) {

    if (balance === "") {
        balance = 0;
    }
    if (percentage === "") {
        percentage = 0;
    }

    var purchaseamount = (percentage / 100) * balance;


    return purchaseamount;
}

function CalculateProfit(saleprice, totalprice, purchasepercentage) {

    if (saleprice === "") {
        saleprice = 0;
    }

    if (totalprice === "") {
        totalprice = 0;
    }

    if (purchasepercentage === "") {
        purchasepercentage = 0;
    }

    var saledifference = totalprice - saleprice;

    var getprofit = purchasepercentage / 100 * saledifference;

    return getprofit;

}

$("#txtinvoicepurchaseamt").bind('keyup mouseup', function () {

    var balance = $("#txtinvoicebalance").val();

    var amount = $("#txtinvoicepurchaseamt").val();

    var totalprice = $("#txtinvoiceamount").val();

    var price = CalculateInvoicePurchasePercentage(balance, amount);

    var profit = CalculateProfit(balance, totalprice, price);

    $("#txtinvoicepurchasedisc").val(price);

    $("#txtpurchaseprofit").val(profit);

});
$("#txtinvoicepurchasedisc").bind('keyup mouseup', function () {

    var invoicediscount = $("#txtinvoicepurchasedisc").val();

    var invoicebalance = $("#txtinvoicebalance").val();

    var totalprice = $("#txtinvoiceamount").val();

    var amount = CalculateInvoicePurchaseAmount(invoicebalance, invoicediscount);

    var profit = CalculateProfit(invoicebalance, totalprice, invoicediscount);


    $("#txtinvoicepurchaseamt").val(amount);

    $("#txtpurchaseprofit").val(profit);

});
