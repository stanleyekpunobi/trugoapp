﻿var apiUrl = 'http://apitest.trugo.co/';
$("#btnpostinvoice").click(function () {
    $("#loader").addClass('is-active');
    //$("#loader").attr('data-text', 'Setting up your TruGo profile');
    //$("#loader").attr('data-blink');
    var getUserDetails = localStorage.getItem('userdata');
    if (getUserDetails === null || getUserDetails === undefined) {
        swal("TruGo", "Unauthorized user access", "error").then(() => {
            window.location.replace("http://apptest.trugo.co/Account/Login");
        });
    }
    else {
        var form = $("#submit_invoiceform")[0];
        var data = new FormData(form);
        data.append("profileid", JSON.parse(getUserDetails).userId);
        console.log(data);
        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: apiUrl + 'api/Seller/PostInvoice',
            data: data,
            processData: false,
            headers:
            {
                'emailurl': 'http://apptest.trugo.co/MarketPlace/Viewinvoice'
            },
            contentType: false,
            cache: false,
            success: function (data) {
                console.log(data);
                if (data !== undefined || data !== null) {
                    $("#loader").removeClass('is-active');
                    if (data.status === true) {
                        swal("TruGo", "Invoice successfuly created and under moderation").then(() => {
                            window.location.replace("http://apptest.trugo.co/Seller/Dashboard");
                        });
                    }
                    else {
                        swal("TruGo", "An error occured", "error");
                    }
                }
            },
            error: function (code, exception) {
                $("#loader").removeClass('is-active');
                if (code.status === 0) {
                    swal("TruGo", "You are not connected to the internet", "error");
                } else if (code.status === 404) {
                    swal("TruGo", "an error has occured", "error");
                } else if (code.status === 500) {
                    swal("TruGo", "an error has occured", "error");
                } else if (exception === 'parsererror') {
                    swal("TruGo", "an error has occured", "error");
                } else if (exception === 'timeout') {
                    swal("TruGo", "Request has timed out", "error");
                } else if (exception === 'abort') {
                    swal("TruGo", "an error has occured", "error");
                } else {
                    swal("TruGo", "an error has occured", "error");
                }

            }
        });

    }
});
$("#inputtag").tagsinput('items', {
    trimValue: true,
    allowDuplicates: false
});
$('#invoicedate').datepicker({
    uiLibrary: 'bootstrap4'
});
$('#invoiceduedate').datepicker({
    uiLibrary: 'bootstrap4'
});
$("#input-id").fileinput(
    {
        'previewFileType': 'any',
        'showUploadedThumbs': true,
        'showRemove': false,
        'showUpload': false,
        'hideThumbnailContent': true,
        'allowedFileTypes': ['image', 'text'],
        'allowedFileExtensions': ['jpg', 'png', 'txt', 'pdf'],
        'maxFileSize': 4000
    }
);
function CalculateInvoiceDiscount(invoicetotal, discount) {

    if (invoicetotal === "") {
        invoicetotal = 0;
    }
    if (discount === "") {
        discount = 0;
    }

    var percentagediscount = discount / 100;

    var discountprice = invoicetotal * percentagediscount;

    var totalprice = invoicetotal - discountprice;

    return totalprice;
}

$("#txtinvoiceamount").bind('keyup mouseup', function () {

    var invoicediscount = $("#txtinvoicedisc").val();

    var invoiceamount = $("#txtinvoiceamount").val();

    var price = CalculateInvoiceDiscount(invoiceamount, invoicediscount);


    $("#txtinvoicesaleprice").val(price);

});
$("#txtinvoicedisc").bind('keyup mouseup', function () {

    var invoicediscount = $("#txtinvoicedisc").val();

    var invoiceamount = $("#txtinvoiceamount").val();

    var price = CalculateInvoiceDiscount(invoiceamount, invoicediscount);


    $("#txtinvoicesaleprice").val(price);

});

//$("#btnsubmit").click(function () {
//    toastr.success('Your invoice has been created and under moderation by TruGo suppoort team', 'Invoice successfuly created', { timeOut: 4000 });

//    setTimeout(function () {
//        window.location.replace("http://localhost:64178/Seller/FeatureInvoice");
//    }, 5000)
//});
