﻿var apiUrl = 'http://apitest.trugo.co/';
$("#accountsetupbtn").click(function () {
    $("#loader").addClass('is-active');
    $("#loader").attr('data-text', 'Setting up your TruGo profile');
    $("#loader").attr('data-blink');
    var getUserDetails = localStorage.getItem('userdata');
    if (getUserDetails === null || getUserDetails === undefined) {
        swal("TruGo", "Unauthorized user access", "error").then(() => {
            window.location.replace("http://apptest.trugo.co/Account/Login");
        });
    }
    else {
        var form = $("#submit_form")[0];
        var data = new FormData(form);
        data.append("userid", JSON.parse(getUserDetails).userId);
        console.log(data);

        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: apiUrl + 'api/Account/OnboardUser',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                console.log(data);
                if (data !== undefined || data !== null) {
                    $("#loader").removeClass('is-active');
                    if (data.status === true) {
                        swal("TruGo", "Your TruGo profile has been created and under moderation").then(() => {
                            window.location.replace("http://apptest.trugo.co/Account/Login");
                        });
                    }
                    else {
                        swal("TruGo", "Profile already exists");
                    }
                }
            },
            error: function (code, exception) {
                $("#loader").removeClass('is-active');
                if (code.status === 0) {
                    swal("TruGo", "You are not connected to the internet", "error");
                } else if (code.status === 404) {
                    swal("TruGo", "an error has occured", "error");
                } else if (code.status === 500) {
                    swal("TruGo", "an error has occured", "error");
                } else if (exception === 'parsererror') {
                    swal("TruGo", "an error has occured", "error");
                } else if (exception === 'timeout') {
                    swal("TruGo", "Request has timed out", "error");
                } else if (exception === 'abort') {
                    swal("TruGo", "an error has occured", "error");
                } else {
                    swal("TruGo", "an error has occured", "error");
                }

            }
        });

    }
});