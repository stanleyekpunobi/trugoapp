﻿var apiUrl = 'http://apitest.trugo.co/';
$("#btnsubmit").click(function () {
    $("#loader").addClass('is-active');
    var amount = $("#txtwithdrawamount").val();
    var date = $("#duedate").val();
    var getUserDetails = localStorage.getItem('userdata');
    var userData = JSON.parse(getUserDetails);
    console.log(userData);

    var requestObject = {
        Amount: amount, Date_Estimate: date, ProfileId: userData.userId
    };

    console.log(requestObject);
    $.ajax({
        type: 'POST',
        url: apiUrl + 'api/TrugoCredit/WithdrawCredit',
        data: JSON.stringify(requestObject),
        contentType: 'application/json',
        success: function (data) {
            console.log(data);
            if (data !== undefined) {
                $("#loader").removeClass('is-active');
                localStorage.setItem('userdata', JSON.stringify(data));
                swal("TruGo!", "Your credit withdrawal request has been created and pending approval");
            }
            console.log(data);
        },
        error: function (code, exception) {
            $("#loader").removeClass('is-active');

            if (code.status === 0) {
                swal("TruGo", "You are not connected to the internet", "error");
            } else if (code.status === 404) {
                swal("TruGo", "an error has occured", "error");
            } else if (code.status === 500) {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'parsererror') {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'timeout') {
                swal("TruGo", "Request has timed out", "error");
            } else if (exception === 'abort') {
                swal("TruGo", "an error has occured", "error");
            } else {

                swal("TruGo", "an error has occured", "error");
            }

        }
    });

});
$('#duedate').datepicker({
    uiLibrary: 'bootstrap4'
});
$("#creditpurchase").change(function () {

    creditpurchaseoptions = $("#creditpurchase").val();

    if (creditpurchaseoptions === "others") {
        $("#interbanktrans").addClass("d-none").removeClass("d-block");
        $("#others").removeClass("d-none").addClass("d-block");

    }
    else {
        $("#others").addClass("d-none").removeClass("d-block");
        $("#interbanktrans").removeClass("d-none").addClass("d-block");
    }

});