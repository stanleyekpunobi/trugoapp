﻿"use strict";
var apiUrl = 'http://apitest.trugo.co';

var getUserDetails = localStorage.getItem('userdata');
var userData = JSON.parse(getUserDetails);

var monthlylabels = [];
var monthlyinvestmentData = [];
var monthlyinvestmentearning = [];

var quarterlabels = [];

var quarterdatainvestments = [];
var quarterdataearnings = [];

var halfyearlabels = [];
var halfyearearnings = [];
var halfyearpayments = [];

var fullyearlabels = [];
var fullyearearnings = [];
var fullyearpayments = [];

//to get quarterly data
var x, j, temparray, chunk = 3;
var quarterlydatainvoicessold = [];
var quarterlydatainvoicestotal = [];

$(document).ready(function () {
    //ajax starts here
    $.ajax({
        type: 'Get',
        url: apiUrl + '/api/Seller/GetDashboarChartdData?userid=' + userData.userId,
        success: function (chartdata) {
            console.log(chartdata);
            //get data on success function and pass it to the chart

            //get monthly labels
            for (var i = 0; i < chartdata.MonthlyLabels.length; i++) {
                var d = chartdata.MonthlyLabels[i];
                d = d.split('T')[0];
                //console.log(d);
                monthlylabels.push(d);
            }

            //get monthly investmentdata
            for (var mi = 0; mi < chartdata.ThisMonthChartData.length; mi++) {
                if (chartdata.ThisMonthChartData[mi].ChartType === 'numberofinvoices') {
                    //var dv = chartdata.MonthlyLabels[mi];
                    //chartdata.ThisMonthChartData[mi].DateValue = dv.split('T')[0];
                    monthlyinvestmentData.push(chartdata.ThisMonthChartData[mi].DataValue);

                }
            }

            //get monthly earning data
            for (var me = 0; me < chartdata.ThisMonthChartData.length; me++) {
                if (chartdata.ThisMonthChartData[me].ChartType === 'soldinvoices') {
                    //var dt = chartdata.MonthlyLabels[me];
                    //chartdata.ThisMonthChartData[me].DateValue = dt.split('T')[0];
                    monthlyinvestmentearning.push(chartdata.ThisMonthChartData[me].DataValue);
                }
            }

            console.log(monthlyinvestmentearning);
            console.log(monthlyinvestmentData);
                        
            var thisMonth = chartdata.ThisMonthChartData[0].Month;
            //get invoices sold by current quater
            for (x = 0, j = 11; x < j; x += chunk) {
                temparray = chartdata.MonthlyChartData.slice(x, x + chunk);
                for (var qta = 0; qta < temparray.length; qta++) {
                    if (temparray[qta].Month === thisMonth) {
                        quarterlydatainvoicessold.push(temparray);
                    }
                }

            }

            temparray.length = 0;

            for (x = 12, j = 23; x < j; x += chunk) {

                temparray = chartdata.MonthlyChartData.slice(x, x + chunk);
                for (var qtas = 0; qtas < temparray.length; qtas++) {
                    if (temparray[qtas].Month === thisMonth) {
                        quarterlydatainvoicestotal.push(temparray);
                    }
                }

            }

            console.log(quarterlydatainvoicessold);
            console.log(quarterlydatainvoicestotal);

            for (var qil = 0; qil < quarterlydatainvoicessold[0].length; qil++)
            {
                quarterlabels.push(quarterlydatainvoicessold[0][qil].Monthname);
                quarterdataearnings.push(quarterlydatainvoicessold[0][qil].DataValue);
            }

            console.log(quarterlabels);
            console.log(quarterdataearnings);

            for (var qsum = 0; qsum < quarterlydatainvoicestotal[0].length; qsum++)
            {
                quarterdatainvestments.push(quarterlydatainvoicestotal[0][qsum].DataValue);
            }


            for (var hyl = 0; hyl < chartdata.MonthlyChartData.length; hyl++) {

                if (hyl <= 5) {
                    halfyearlabels.push(chartdata.MonthlyChartData[hyl].Monthname);
                }

                if (hyl <= 5 && chartdata.MonthlyChartData[hyl].ChartType === 'numberofinvoices') {
                    halfyearearnings.push(chartdata.MonthlyChartData[hyl].DataValue);
                }

                if (hyl >= 12 && hyl <= 17 && chartdata.MonthlyChartData[hyl].ChartType === 'soldinvoices') {
                    halfyearpayments.push(chartdata.MonthlyChartData[hyl].DataValue);
                }

            }

            for (var fyl = 0; fyl < chartdata.MonthlyChartData.length; fyl++) {

                if (fyl <= 11) {
                    fullyearlabels.push(chartdata.MonthlyChartData[fyl].Monthname);

                }

                if (fyl <= 11 && chartdata.MonthlyChartData[fyl].ChartType === 'numberofinvoices') {
                    fullyearearnings.push(chartdata.MonthlyChartData[fyl].DataValue);
                }

                if (fyl >= 12 && chartdata.MonthlyChartData[fyl].ChartType === 'soldinvoices') {
                    fullyearpayments.push(chartdata.MonthlyChartData[fyl].DataValue);
                }

            }

            var ctx = $("#month");
            var lineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: monthlylabels,
                    datasets: [
                        {
                            label: "Number of invoices",
                            data: monthlyinvestmentData,
                            backgroundColor: "rgba(0,174,255,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true
                        },
                        {
                            label: "Invoices sold",
                            data: monthlyinvestmentearning,
                            backgroundColor: "rgba(50,210,201,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true
                        }
                    ]
                },
                options: {
                    legend: {
                        display: true,
                        position: 'top',
                        reverse: true,
                        labels: {
                            fontColor: '#222'
                        }
                    }
                }
            });

            var quarter = $("#quarter");
            var chartQuarter = new Chart(quarter, {
                type: 'line',
                data: {
                    labels: quarterlabels,
                    datasets: [
                        {
                            label: "Invoices sold",
                            data: quarterdatainvestments,
                            backgroundColor: "rgba(0,174,255,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        },
                        {
                            label: "Number of invoices",
                            data: quarterdataearnings,
                            backgroundColor: "rgba(50,210,201,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        }
                    ]
                }
            });

            var halfYear = $("#half-year");
            var chartHalfYear = new Chart(halfYear, {
                type: 'line',
                data: {
                    labels: halfyearlabels,
                    datasets: [
                        {
                            label: "Invoices sold",
                            data: halfyearpayments,
                            backgroundColor: "rgba(0,174,255,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        },
                        {
                            label: "Number of invoices",
                            data: halfyearearnings,
                            backgroundColor: "rgba(50,210,201,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        }
                    ]
                }
            });

            var year = $("#year");
            var chartYear = new Chart(year, {
                type: 'line',
                data: {
                    labels: fullyearlabels,
                    datasets: [
                        {
                            label: "Invoices sold",
                            data: fullyearpayments,
                            backgroundColor: "rgba(0,174,255,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true
                        },
                        {
                            label: "Number of invoices",
                            data: fullyearearnings,
                            backgroundColor: "rgba(50,210,201,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true
                        }
                    ]
                }
            });

        }
    });

});
