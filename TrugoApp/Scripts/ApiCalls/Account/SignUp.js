﻿var apiUrl = 'http://apitest.trugo.co/';
$("#btncreateaccount").click(function () {
    $("#loader").addClass('is-active');
    var EmailAddress = $("#txtemail").val();
    var Password = $("#txtpassword").val();
    var ConfirmPassword = $("#txtconfirmpassword").val();
    var AccountType = $("input[name='selector']:checked").val();
    var requestObject = {
        Email: EmailAddress, Password: Password, ConfirmPassword: ConfirmPassword, Role: AccountType
    };
    console.log(requestObject);
    $.ajax({
        type: 'POST',
        headers:
        {
            'emailurl': 'http://apptest.trugo.co/Account/VerifyUser'
        },
        url: apiUrl + '/api/Account/Register',
        data: JSON.stringify(requestObject),
        contentType: 'application/json',
        success: function (data) {
            console.log(data);
            if (data !== undefined) {
                $("#loader").removeClass('is-active');
                localStorage.setItem('userdata', JSON.stringify(data));
                swal("TruGo!", "User account created, check your email for verification link");
            }
            console.log(data);
        },
        error: function (code, exception) {
            $("#loader").removeClass('is-active');

            if (code.status === 0) {
                swal("TruGo", "You are not connected to the internet", "error");
            } else if (code.status === 404) {
                swal("TruGo", "an error has occured", "error");
            } else if (code.status === 500) {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'parsererror') {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'timeout') {
                swal("TruGo", "Request has timed out", "error");
            } else if (exception === 'abort') {
                swal("TruGo", "an error has occured", "error");
            } else {
                swal("TruGo", "an error has occured", "error");
            }

        }
    });
});
$("#btnresendactivationemail").click(function () {
    $("#loader").addClass('is-active');
    var userid = getUrlParameter('userid');
    var requestObject = {
        UserId: userid
    };
    $.ajax({
        type: 'POST',
        headers:
        {
            'emailurl': 'http://apptest.trugo.co/Account/VerifyUser'
        },
        url: apiUrl + '/api/Account/ResendActivationEmail',
        data: JSON.stringify(requestObject),
        contentType: 'application/json',
        success: function (data) {
            console.log(data);
            if (data !== undefined) {
                $("#loader").removeClass('is-active');
                swal("TruGo!", "Email activation link sent, check your email for verification link");
            }
            console.log(data);
        },
        error: function (code, exception) {
            $("#loader").removeClass('is-active');

            if (code.status === 0) {
                swal("TruGo", "You are not connected to the internet", "error");
            } else if (code.status === 404) {
                swal("TruGo", "an error has occured", "error");
            } else if (code.status === 500) {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'parsererror') {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'timeout') {
                swal("TruGo", "Request has timed out", "error");
            } else if (exception === 'abort') {
                swal("TruGo", "an error has occured", "error");
            } else {
                swal("TruGo", "an error has occured", "error");
            }

        }
    });

});
$("#btnresetpassword").click(function () {
    $("#loader").addClass('is-active');
    var Email = $("#txtresetemail").val();
    var requestObject = {
        Email: Email
    };
    $.ajax({
        type: 'POST',
        headers:
        {
            'emailurl': 'http://apptest.trugo.co/Account/ResetPassword'
        },
        url: apiUrl + '/api/Account/InitiatePasswordChange',
        data: JSON.stringify(requestObject),
        contentType: 'application/json',
        success: function (data) {
            console.log(data);
            if (data !== undefined) {
                $("#loader").removeClass('is-active');
                swal("TruGo!", "Email password reset link sent, check your email for password reset link");
            }
            console.log(data);
        },
        error: function (code, exception) {
            $("#loader").removeClass('is-active');

            if (code.status === 0) {
                swal("TruGo", "You are not connected to the internet", "error");
            } else if (code.status === 404) {
                swal("TruGo", "an error has occured", "error");
            } else if (code.status === 500) {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'parsererror') {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'timeout') {
                swal("TruGo", "Request has timed out", "error");
            } else if (exception === 'abort') {
                swal("TruGo", "an error has occured", "error");
            } else {
                swal("TruGo", "an error has occured", "error");
            }

        }
    });
});

$("#btnchangepassword").click(function () {
    $("#loader").addClass('is-active');
    var password = $("#txtnewpassword").val();
    var confirmpassword = $("#txtconfirmpassword").val();
    var userid = getUrlParameter('userid');
    var code = getUrlParameter('code');

    var requestObject = {
        Code: code, UserId: userid, NewPassword: password, ConfirmPassword: confirmpassword
    };
    $.ajax({
        type: 'POST',
        url: apiUrl + '/api/Account/ResetPassword',
        data: JSON.stringify(requestObject),
        contentType: 'application/json',
        success: function (data) {
            console.log(data);
            if (data !== undefined || data !== null) {
                $("#loader").removeClass('is-active');
                swal("TruGo!", "Password reset successful");
                setTimeout(function () {
                    window.location.replace("http://apptest.trugo.co/Account/Login");
                }, 2000);
            }
            console.log(data);
        },
        error: function (code, exception) {
            $("#loader").removeClass('is-active');
            if (code.status === 0) {
                swal("TruGo", "You are not connected to the internet", "error");
            }
            else if (code.status === 404) {
                swal("TruGo", "an error has occured", "error");
            } else if (code.status === 500) {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'parsererror') {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'timeout') {
                swal("TruGo", "Request has timed out", "error");
            } else if (exception === 'abort') {
                swal("TruGo", "an error has occured", "error");
            } else {
                swal("TruGo", "an error has occured", "error");
            }

        }
    });
});

$("#btnsignin").click(function () {
    $("#loader").addClass('is-active');
    var email = $("#txtsigninemail").val();
    var password = $("#txtsigninpassword").val();

    var requestObject = {
        Email: email, Password: password
    };
    $.ajax({
        type: 'POST',
        url: apiUrl + '/api/Account/Login',
        data: JSON.stringify(requestObject),
        contentType: 'application/json',
        success: function (data) {
            console.log(data);
            if (data !== undefined && data !== null) {
                localStorage.setItem('userdata', JSON.stringify(data));
                console.log(data);
                $("#loader").removeClass('is-active');
                if (data.emailconfirmed.toLowerCase() === 'false') {
                    swal("TruGo!", "Your TruGo email has not been verified", "error");
                    setTimeout(function () {
                        window.location.replace("http://apptest.trugo.co/Account/VerifyUser?userid=" + data.userId);
                    }, 2000);
                }
                else if (data.profilecreated.toLowerCase() === 'false') {
                    swal("TruGo!", "You have not set up your TruGo profile, click Ok to get started", "error").then(() => {
                        window.location.replace("http://apptest.trugo.co/Account/AccountSetup");
                    });
                }
                else if (data.accountapproved.toLowerCase() === 'false') {
                    swal("TruGo!", "Your TruGo profile is still on moderation", "error");
                    localStorage.clear();
                }
                else
                {
                    var getUserDetails = localStorage.getItem('userdata');
                    var userData = JSON.parse(getUserDetails);

                 $.ajax({

                     type: 'Get',
                     url: apiUrl + '/api/Account/GetUserRoles?userid=' + userData.userId,
                        success: function (userrole) {
                            if (userrole !== undefined && userrole !== null) {
                                console.log(userrole);
                                for (var i = 0; i < userrole.length; i++) {
                                    if (userrole[i] === 'seller') {
                                        window.location.replace("http://apptest.trugo.co/Seller/Dashboard?userid=" + userData.userId);

                                    }
                                    else if (userrole[i] === 'investor') {

                                        window.location.replace("http://apptest.trugo.co/Seller/Dashboard?userid=" + userData.userId);
                                    }
                                    else {
                                       // return null;
                                    }
                                }

                            }
                            else {
                                swal("TruGo!", "Invalid login details", "error");
                            }
                        }
                    });
                }
            }
        },
        error: function (code, exception) {
            $("#loader").removeClass('is-active');
            if (code.status === 0) {
                swal("TruGo", "You are not connected to the internet", "error");
            } else if (code.status === 404) {
                swal("TruGo", "an error has occured", "error");
            } else if (code.status === 500) {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'parsererror') {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'timeout') {
                swal("TruGo", "Request has timed out", "error");
            } else if (exception === 'abort') {
                swal("TruGo", "an error has occured", "error");
            } else {
                swal("TruGo", "an error has occured", "error");
            }

        }
    });
});

$("#btnfacebooksignup").click(function () {
    $.ajax({
        type: 'Get',
        url: apiUrl + '/api/Account/ExternalLogins?returnUrl=http://localhost:64178/Account/RegisterSocialAuth&generateState=true',
        success: function (data) {
            if (data !== undefined && data !== null) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Name === 'Facebook') {
                        //$.ajax({
                        //    type: 'Get',
                        //    url: apiUrl + data[i].Url,
                        //    success: function () {
                        //    }
                        //});
                        window.location.replace(apiUrl + data[i].Url);
                    }
                }

            }
        },
        error: function (code, exception) {
            $("#loader").removeClass('is-active');
            if (code.status === 0) {
                swal("TruGo", "You are not connected to the internet", "error");
            } else if (code.status === 404) {
                swal("TruGo", "an error has occured", "error");
            } else if (code.status === 500) {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'parsererror') {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'timeout') {
                swal("TruGo", "Request has timed out", "error");
            } else if (exception === 'abort') {
                swal("TruGo", "an error has occured", "error");
            } else {
                swal("TruGo", "an error has occured", "error");
            }
        }

    });

});

$("#btngooglesignup").click(function () {
    $.ajax({
        type: 'Get',
        url: apiUrl + '/api/Account/ExternalLogins?returnUrl=http://localhost:64178/Account/RegisterSocialAuth?&generateState=true',
        success: function (data) {
            if (data !== undefined && data !== null) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Name === 'Google') {

                        window.location.replace(apiUrl + data[i].Url);
                    }
                }

            }
        },
        error: function (code, exception) {
            $("#loader").removeClass('is-active');
            if (code.status === 0) {
                swal("TruGo", "You are not connected to the internet", "error");
            } else if (code.status === 404) {
                swal("TruGo", "an error has occured", "error");
            } else if (code.status === 500) {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'parsererror') {
                swal("TruGo", "an error has occured", "error");
            } else if (exception === 'timeout') {
                swal("TruGo", "Request has timed out", "error");
            } else if (exception === 'abort') {
                swal("TruGo", "an error has occured", "error");
            } else {
                swal("TruGo", "an error has occured", "error");
            }
        }

    });
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};