﻿"use strict";
var apiUrl = 'http://apitest.trugo.co';

var getUserDetails = localStorage.getItem('userdata');
var userData = JSON.parse(getUserDetails);

var monthlylabels = [];
var monthlyinvestmentData = [];
var monthlyinvestmentearning = [];

var quarterlabels = [];

var quarterdatainvestments = [];
var quarterdataearnings = [];

var halfyearlabels = [];
var halfyearearnings = [];
var halfyearpayments = [];

var fullyearlabels = [];
var fullyearearnings = [];
var fullyearpayments = [];

$(document).ready(function () {
    //ajax starts here
    $.ajax({
        type: 'Get',
        url: apiUrl + '/api/Investor/GetDashboarChartdData?userid=' + userData.userId,
        success: function (chartdata) {
            console.log(chartdata);
            //get data on success function and pass it to the chart

            //get monthly labels
            for (var i = 0; i < chartdata.MonthlyLabels; i++) {
                monthlylabels.push[i];
            }

            //get monthly investmentdata
            for (var mi = 0; mi < chartdata.ThisMonthChartData; mi++) {
                if (chartdata.ThisMonthChartData[mi].ChartType === 'investormonthlypurchasesum') {
                    monthlyinvestmentData.push[chartdata.ThisMonthChartData[i].DataValue];

                }
            }

            //get monthly earning data
            for (var me = 0; me < chartdata.ThisMonthChartData; me++) {
                if (chartdata.ThisMonthChartData[me].ChartType === 'investormonthlypurchasecount') {
                    monthlyinvestmentearning.push[chartdata.ThisMonthChartData[i].DataValue];
                }
            }

            for (var qil = 0; qil < chartdata.MonthlyChartData.length; qil++) {

                if (qil <= 2) {
                    quarterlabels.push(chartdata.MonthlyChartData[qil].Monthname);

                }

                if (qil <= 2 && chartdata.MonthlyChartData[qil].ChartType === 'count') {
                    quarterdataearnings.push(chartdata.MonthlyChartData[qil].DataValue);
                }

                if (qil <= 2 && chartdata.MonthlyChartData[qil].ChartType === 'sum') {
                    quarterdatainvestments.push(chartdata.MonthlyChartData[qil].DataValue);
                }
              
            }

            for (var hyl = 0; hyl < chartdata.MonthlyChartData.length; hyl++) {

                if (hyl <= 5) {
                    halfyearlabels.push(chartdata.MonthlyChartData[hyl].Monthname);

                }

                if (hyl <= 5 && chartdata.MonthlyChartData[hyl].ChartType === 'count') {
                    halfyearearnings.push(chartdata.MonthlyChartData[hyl].DataValue);
                }

                if (hyl <= 5 && chartdata.MonthlyChartData[hyl].ChartType === 'sum') {
                    halfyearpayments.push(chartdata.MonthlyChartData[hyl].DataValue);
                }

            }

            for (var fyl = 0; fyl < chartdata.MonthlyChartData.length; fyl++) {

                if (fyl <= 11) {
                    fullyearlabels.push(chartdata.MonthlyChartData[fyl].Monthname);

                }

                if (fyl <= 11 && chartdata.MonthlyChartData[fyl].ChartType === 'count') {
                    fullyearearnings.push(chartdata.MonthlyChartData[fyl].DataValue);
                }

                if (fyl <= 11 && chartdata.MonthlyChartData[fyl].ChartType === 'sum') {
                    fullyearpayments.push(chartdata.MonthlyChartData[fyl].DataValue);
                }

            }

            var ctx = $("#month");
            var lineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: monthlylabels,
                    datasets: [
                        {
                            label: "Investments",
                            data: monthlyinvestmentData,
                            backgroundColor: "rgba(0,174,255,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true
                        },
                        {
                            label: "Earning",
                            data: monthlyinvestmentearning,
                            backgroundColor: "rgba(50,210,201,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true
                        }
                    ]
                },
                options: {
                    legend: {
                        display: true,
                        position: 'top',
                        reverse: true,
                        labels: {
                            fontColor: '#222'
                        }
                    }
                }
            });

            var quarter = $("#quarter");
            var chartQuarter = new Chart(quarter, {
                type: 'line',
                data: {
                    labels: quarterlabels,
                    datasets: [
                        {
                            label: "Payments",
                            data: quarterdatainvestments,
                            backgroundColor: "rgba(0,174,255,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        },
                        {
                            label: "Earning",
                            data: quarterdataearnings,
                            backgroundColor: "rgba(50,210,201,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        }
                    ]
                }
            });

            var halfYear = $("#half-year");
            var chartHalfYear = new Chart(halfYear, {
                type: 'line',
                data: {
                    labels: halfyearlabels,
                    datasets: [
                        {
                            label: "Payments",
                            data: halfyearpayments,
                            backgroundColor: "rgba(0,174,255,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        },
                        {
                            label: "Earning",
                            data: halfyearearnings,
                            backgroundColor: "rgba(50,210,201,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        }
                    ]
                }
            });

            var year = $("#year");
            var chartYear = new Chart(year, {
                type: 'line',
                data: {
                    labels: fullyearlabels,
                    datasets: [
                        {
                            label: "Payments",
                            data: fullyearpayments,
                            backgroundColor: "rgba(0,174,255,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        },
                        {
                            label: "Earning",
                            data: fullyearearnings,
                            backgroundColor: "rgba(50,210,201,0.6)",
                            borderColor: "transparent",
                            pointRadius: 0,
                            spanGaps: true,
                        }
                    ]
                }
            });

        }
    });

});
