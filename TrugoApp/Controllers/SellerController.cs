﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrugoApp.Models.DalClasses;

namespace TrugoApp.Controllers
{
    public class SellerController : Controller
    {

        private SellerDal _sellerDal;

        public SellerDal SellerDal
        {
            get
            {
                return new SellerDal();
            }

            set
            {
                _sellerDal = value;
            }
        }

        public SellerController(SellerDal sellerDal)
        {
            SellerDal = sellerDal;
        }

        public SellerController()
        {

        }

        // GET: Seller
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard(string userid)
        {
            var dashboardData = SellerDal.GetSellerDashboardData(userid);

            if (dashboardData == null)
            {
                return View();
            }
            else
            {
                return View(dashboardData);

            }
        }

        public ActionResult PostInvoice()
        {
            return View();
        }

        public ActionResult FeatureInvoice()
        {
            return View();
        }
    }
}