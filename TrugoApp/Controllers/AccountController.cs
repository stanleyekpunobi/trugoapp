﻿using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using TrugoApp.Models;
using TrugoApp.Models.Classes;
using TruGoClassLibrary.Classes;
namespace TrugoApp.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult RegisterSocialAuth()
        {
            return View();
        }

        public ActionResult VerifyUser(string userid, string code)
        {
            var client = new RestClient(EnvironmentClass.ApiBaseUrl);

            var request = new RestRequest("api/Account/ConfirmEmail", Method.POST);
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserId", userid);
            parameters.Add("Code", code);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);
            var response = client.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContentClass>(response.Content);
            if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK && responseContent.Status == true)
            {
                Session["emailverified"] = "true";
                return RedirectToAction("AccountSetup");
            }
            else
            {
                return View();
            }
        }

        public ActionResult WithdrawCredit()
        {
            return View();
        }

        public ActionResult AccountStatement()
        {
            return View();
        }

        public ActionResult PurchaseCredit()
        {
            return View();
        }

        public ActionResult Signup()
        {
            return View();
        }

        public ActionResult AccountSettings()
        {
            return View();
        }

        public ActionResult ResetPassword(string userid, string code)
        {
            if (Session["resetverified"] != null)
            {
                ViewBag.resetverified = "true";
                Session.Remove("resetverified");
            }
           
            if (string.IsNullOrEmpty(userid) && string.IsNullOrEmpty(code))
            {
                Session["resetverified"] = "true";
                ViewBag.resetverified = "true";
                return View();
            }
            else
            {
                return View();
            }
        }

        public ActionResult AccountSetup()
        {
            //if (Session["emailverified"] != null)
            //{
            //    ViewBag.emailverified = "true";
            //    Session.Remove("emailverified");
            //    return View();

            //}
            //else
            //{
            //    return RedirectToAction("Login");
            //}

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AccountSetup(string email)
        {
            return View();
        }
    }
}