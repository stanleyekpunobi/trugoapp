﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrugoApp.Models.DalClasses;

namespace TrugoApp.Controllers
{
    public class InvestorController : Controller
    {

        private InvestorDal _investorDal;

        public InvestorDal InvestorDal
        {
            get
            {
                return new InvestorDal();
            }

            set
            {
                _investorDal = value;
            }
        }

        public InvestorController(InvestorDal investorDal)
        {
            InvestorDal = investorDal;
        }

        public InvestorController()
        {

        }

        // GET: Investor
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard(string userid)
        {
            var dashboardData = InvestorDal.GetInvestorDashboardData(userid);

            if (dashboardData == null)
            {
                return View();
            }
            else
            {
                return View(dashboardData);

            }
        }
    }
}