﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrugoApp.Models.DalClasses;
using TruGoClassLibrary.RequestModels;
using PagedList;
using TruGoClassLibrary.ResponseClass;

namespace TrugoApp.Controllers
{
    public class MarketPlaceController : Controller
    {

        private MarketPlaceDal _marketPlaceDal;

        public MarketPlaceDal MarketPlaceDal
        {
            get
            {
                return new MarketPlaceDal();
            }

            set
            {
                _marketPlaceDal = value;
            }
        }

        public MarketPlaceController()
        {

        }

        public MarketPlaceController(MarketPlaceDal marketPlaceDal)
        {
            MarketPlaceDal = marketPlaceDal;
        }

        // GET: MarketPlace
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Invoices(int? page, InvoiceSearchRequestModel searchModel)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? (int)page : 1;
            IPagedList<InvoiceResponseModel> invoices = null;
            var getInvoices = MarketPlaceDal.LoadInvoices();
            if (searchModel != null)
            {
                getInvoices = MarketPlaceDal.FilterInvoices(getInvoices, searchModel);
            }
            invoices = getInvoices.ToPagedList(pageIndex, pageSize);
            return View(invoices);
        }

        //[HttpPost]
        //public ActionResult FilterInvoices(InvoiceSearchRequestModel model)
        //{
        //    var searchResponse = MarketPlaceDal.FilterInvoices(model);

        //    return RedirectToAction("Invoices");
        //}
        public ActionResult ViewInvoice(int? invoiceid, string code)
        {
            var invoiceDetail = MarketPlaceDal.GetInvoiceDetail(invoiceid, code);

            return View(invoiceDetail);
        }
        public ActionResult Purchaseinvoice(int? invoiceid, string code)
        {
            var invoiceDetail = MarketPlaceDal.GetInvoiceDetail(invoiceid, code);

            return View(invoiceDetail);
        }
        public ActionResult RaiseInvoiceDispute()
        {
            return View();
        }

        
    }
}